import projects from './static/data/projects.json'

export default {
  target: "static",
  head: {
    title: "Etienne Moureton - Portfolio",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { name: "format-detection", content: "telephone=no" },
      {
        name: "description",
        content:
          "Creative developer based in France & working remotely. Specialized in animations, working with Vue and Nuxt.",
      },
      {
        name: "image",
        content: "/img/portrait.webp",
      },
      {
        name: "og:description",
        content:
          "Creative developer based in France & working remotely. Specialized in animations, working with Vue and Nuxt.",
      },
      {
        name: "og:image",
        content: "/img/portrait.webp",
      },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  css: [
    {
      src: "~/assets/styles/global.scss",
      lang: "scss",
    },
    {
      src: "~/assets/styles/_fonts.scss",
      lang: "scss",
    },
    {
      src: "~/assets/styles/_variables.scss",
      lang: "scss",
    },
    {
      src: "~/assets/styles/_transitions.scss",
      lang: "scss",
    },
  ],

  plugins: [],

  components: [{ path: "~/components", extensions: ["vue"] }],

  buildModules: ["nuxt-gsap-module"],

  modules: [
    "@nuxtjs/svg",
    "@nuxt/image",
    "@nuxtjs/sitemap",
    "@nuxt/http",
    "vue-plausible",
  ],

  axios: {
    baseURL: "/",
  },

  build: {},

  env: {
    APP_URL: process.env.APP_URL || "http://etiennemoureton.fr",
  },

  sitemap: {
    hostname: process.env.APP_URL,
    routes: async () => {
      const routes = projects
        .filter((el) => el.url)
        .map((v) => "/projects/" + v.url);
      return routes;
    },
  },

  gsap: {
    clubPlugins: {
      splitText: true,
      drawSVG: true,
    },
    extraPlugins: {
      scrollTrigger: true,
    },
  },

  plausible: {
    domain: "etiennemoureton.fr",
  },
};
 